package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.model.EntityLog;

public interface ILoggerService {

    void writeLog(EntityLog message);

}
