package ru.tsc.chertkova.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.Domain;
import ru.tsc.chertkova.tm.dto.request.data.DataBinarySaveRequest;
import ru.tsc.chertkova.tm.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DomainBinarySaveCommand extends AbstractDomainCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    public static final String DESCRIPTION = "Save date in binary file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        getServiceLocator().getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
