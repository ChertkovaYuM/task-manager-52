package ru.tsc.chertkova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.service.ISenderService;
import ru.tsc.chertkova.tm.model.EntityLog;
import ru.tsc.chertkova.tm.service.SenderService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageExecutor {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ISenderService service = new SenderService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final EntityLog entity = service.createMessage(object, type);
            service.send(entity);
        });
    }

    public void stop() {
        es.shutdown();
    }

}
