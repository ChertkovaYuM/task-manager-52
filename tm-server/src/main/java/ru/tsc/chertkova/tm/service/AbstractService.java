package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IService;
import ru.tsc.chertkova.tm.model.AbstractModel;

public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
