package ru.tsc.chertkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.listener.EntityListener;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@EntityListeners(EntityListener.class)
public final class Session extends AbstractUserOwnerModel {

    private static final long serialVersionUID = 1;

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}
